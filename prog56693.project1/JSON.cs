﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.Json;
using System.IO;

namespace prog56693.project1
{
    // Classes used to serialize/deserialize JSON files
    class GameSettings
    {
        // Game Settings
        public string windowName { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int frameLimit { get; set; }
        public bool fullscreen { get; set; }
        public string background { get; set; }
        public string uiFontPath { get; set; }

        // Entities settings location
        public string playerShip { get; set; }
        public string enemyShip { get; set; }
        public string enemyShipB { get; set; }
        public string enemyShipC { get; set; }
        public string asteroidA { get; set; }
        public string asteroidB { get; set; }
    }

    class ShipSettings
    {
        public int lives { get; set; }
        public int points { get; set; }
        public float speed { get; set; }
        public string texturePath { get; set; }
        public string projectileTexture { get; set; }
    }
}
