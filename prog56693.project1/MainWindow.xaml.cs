﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Text.Json;
using System.IO;

namespace prog56693.project1
{
    enum ObjectType
    {
        Player,
        Enemy,
        Asteroid
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Button> IOButtons = new List<Button>(); // Maybe unnecessary

        List<string> fileNames = new List<string>();

        public MainWindow()
        {
            InitializeComponent();

            IOButtons.Add(readMainFileBut);
            IOButtons.Add(saveMainFileBut);

            IOButtons.Add(readObjectFileBut); 
            IOButtons.Add(saveObjectFileBut);
        }

        //Updates list of files in selected directory
        private void UpdateFiles()
        {
            string[] buffer = Directory.GetFileSystemEntries(SettingsPassTB.Text, "*.json");
            fileNames.Clear();

            foreach (string path in buffer)
            {
                fileNames.Add(System.IO.Path.GetFileName(path));
            }

            ListView_Data.Items.Refresh();

            backgroundCB.Items.Refresh();

            playerFileCB.Items.Refresh();

            enemyAFileCB.Items.Refresh();
            enemyBFileCB.Items.Refresh();
            enemyCFileCB.Items.Refresh();

            asteroidAFileCB.Items.Refresh();
            asteroidBFileCB.Items.Refresh();
        }

        // Assigning fileNames to ComboBoxes and ListView
        private void FilePathButton_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                if(result == System.Windows.Forms.DialogResult.OK)
                {
                    SettingsPassTB.Text = dialog.SelectedPath;

                    ListView_Data.ItemsSource = fileNames;

                    backgroundCB.ItemsSource = fileNames;

                    playerFileCB.ItemsSource = fileNames;

                    enemyAFileCB.ItemsSource = fileNames;
                    enemyBFileCB.ItemsSource = fileNames;
                    enemyCFileCB.ItemsSource = fileNames;

                    asteroidAFileCB.ItemsSource = fileNames;
                    asteroidBFileCB.ItemsSource = fileNames;

                    UpdateFiles();

                    foreach (Button button in IOButtons)
                    {
                        button.IsEnabled = true;
                    }
                }
            }
        }

        private void objectTypeCB_Loaded(object sender, RoutedEventArgs e)
        {
            List<ObjectType> objectTypes = new List<ObjectType>();

            objectTypes.Add(ObjectType.Player);
            objectTypes.Add(ObjectType.Enemy);
            objectTypes.Add(ObjectType.Asteroid);

            objectTypeCB.ItemsSource = objectTypes;
            objectTypeCB.SelectedIndex = 0;
        }

        //This function handles convertion of String to Integer
        private bool ParseTextToInt(string _input, ref int _output)
        {
            int ret = 0;

            try
            {
                ret = Convert.ToInt32(_input);
            }
            catch (OverflowException)
            {
                MessageBox.Show($"{_input} is outside the range of the Int32 type.",
                    "Error", 
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
            catch (FormatException)
            {
                MessageBox.Show($"The value '{_input}' is not in a recognizable format.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
            if (ret < 0)
            {
                MessageBox.Show($"The value '{ret}' can't be less than zero",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
            _output = ret;
            return true;
        }
        // This function handles convertion of String to Float
        private bool ParseTextToFloat(string _input, ref float _output)
        {
            float ret = 0.0f;

            try
            {
                ret = Convert.ToSingle(_input);
            }
            catch (OverflowException)
            {
                MessageBox.Show($"{_input} is outside the range of the Float type.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
            catch (FormatException)
            {
                MessageBox.Show($"The value '{_input}' is not in a recognizable format.",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
            if(ret < 0)
            {
                MessageBox.Show($"The value '{ret}' can't be less than zero",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }

            _output = ret;
            return true;
        }

        //Reading Main settings file
        private void readMainFileBut_Click(object sender, RoutedEventArgs e)
        {
            //I could make it an editable parameter, but I don't think it's necessary
            string filePath = $"{SettingsPassTB.Text}\\GameSettings.json";

            if (!File.Exists(filePath))
            {
                MessageBox.Show($"Couldn't open file at '{filePath}'",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            //Reading json file and deserealizing it
            string json = File.ReadAllText(filePath);
            GameSettings mainSettings = JsonSerializer.Deserialize<GameSettings>(json);

            //Assigning values to interface field
            windowNameTB.Text = mainSettings.windowName;
            widthTB.Text = mainSettings.width.ToString();
            heightTB.Text = mainSettings.height.ToString();
            frameLimitTB.Text = mainSettings.frameLimit.ToString();
            fullscreenCB.IsChecked = mainSettings.fullscreen;
            fontTB.Text = mainSettings.uiFontPath;

            //I have to remove "Settings/" part to find a proper index of file
            backgroundCB.SelectedIndex = fileNames.IndexOf(mainSettings.background.Replace("Settings/", ""));
            playerFileCB.SelectedIndex = fileNames.IndexOf(mainSettings.playerShip.Replace("Settings/", ""));
            enemyAFileCB.SelectedIndex = fileNames.IndexOf(mainSettings.enemyShip.Replace("Settings/", ""));
            enemyBFileCB.SelectedIndex = fileNames.IndexOf(mainSettings.enemyShipB.Replace("Settings/", ""));
            enemyCFileCB.SelectedIndex = fileNames.IndexOf(mainSettings.enemyShipC.Replace("Settings/", ""));
            asteroidAFileCB.SelectedIndex = fileNames.IndexOf(mainSettings.asteroidA.Replace("Settings/", ""));
            asteroidBFileCB.SelectedIndex = fileNames.IndexOf(mainSettings.asteroidB.Replace("Settings/", ""));
        }

        // Saving Main settings file
        private void saveMainFileBut_Click(object sender, RoutedEventArgs e)
        {
            int aWidth = 0;
            int aHeight = 0;
            int aFrameLimit = 0;

            // If something goes wrong - file is not saved
            if (!ParseTextToInt(widthTB.Text, ref aWidth)) return;
            if (!ParseTextToInt(heightTB.Text, ref aHeight)) return;
            if (!ParseTextToInt(frameLimitTB.Text, ref aFrameLimit)) return;

            GameSettings mainSettings = new GameSettings
            {
                windowName = windowNameTB.Text,
                width = aWidth,
                height = aHeight,
                frameLimit = aFrameLimit,
                fullscreen = fullscreenCB.IsChecked == true,
                uiFontPath = fontTB.Text,
                background = "Settings/" + (string)backgroundCB.SelectedItem,
                playerShip = "Settings/" + (string)playerFileCB.SelectedItem,
                enemyShip = "Settings/" + (string)enemyAFileCB.SelectedItem,
                enemyShipB = "Settings/" + (string)enemyBFileCB.SelectedItem,
                enemyShipC = "Settings/" + (string)enemyCFileCB.SelectedItem,
                asteroidA = "Settings/" + (string)asteroidAFileCB.SelectedItem,
                asteroidB = "Settings/" + (string)asteroidBFileCB.SelectedItem
            };

            File.WriteAllText("GameSettings.json", JsonSerializer.Serialize(mainSettings));
        }

        // Reading object-specific file
        private void readObjectFileBut_Click(object sender, RoutedEventArgs e)
        {
            string filePath = $"{SettingsPassTB.Text}\\{fileNameTB.Text}";

            if(!File.Exists(filePath))
            {
                MessageBox.Show($"Couldn't open file at '{filePath}'",
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            string json = File.ReadAllText(filePath);
            ShipSettings shipSettings = JsonSerializer.Deserialize<ShipSettings>(json);

            livesTB.Text = shipSettings.lives.ToString();
            pointsTB.Text = shipSettings.points.ToString();
            speedTB.Text = shipSettings.speed.ToString();
            textureTB.Text = shipSettings.texturePath;
            projectileTB.Text = shipSettings.projectileTexture;
        }

        // Saving object-specific file
        private void saveObjectFileBut_Click(object sender, RoutedEventArgs e)
        {
            int aLives = 0;
            int aPoints = 0;
            float aSpeed = 0.0f;

            if (!ParseTextToInt(livesTB.Text, ref aLives)) return;
            if (!ParseTextToInt(pointsTB.Text, ref aPoints)) return;
            if (!ParseTextToFloat(speedTB.Text, ref aSpeed)) return;

            ShipSettings shipSettings = new ShipSettings
            {
                lives = aLives,
                points = aPoints,
                speed = aSpeed,
                texturePath = textureTB.Text,
                projectileTexture = projectileTB.Text
            };

            File.WriteAllText($"{SettingsPassTB.Text}\\{fileNameTB.Text}", JsonSerializer.Serialize(shipSettings));

            //Updating list of files in directory
            UpdateFiles();
        }

        // This function enables/disables fields depending on selected object type
        // Kinda unnecessary
        private void objectTypeCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ObjectType objectType = (ObjectType)objectTypeCB.SelectedItem;
            switch (objectType)
            {
                case ObjectType.Player:
                    {
                        projectileTB.IsEnabled = true;
                        pointsTB.IsEnabled = false;
                        break;
                    }
                case ObjectType.Asteroid:
                    {
                        projectileTB.IsEnabled = false;
                        pointsTB.IsEnabled = true;
                        break;
                    }
                case ObjectType.Enemy:
                    {
                        projectileTB.IsEnabled = true;
                        pointsTB.IsEnabled = true;
                        break;
                    }
            }
        }
    }
}
