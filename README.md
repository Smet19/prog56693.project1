# Prog56693.Project1

Game Tools and Data Driven Design PROG56693 Project #1 Assignment brief

## Part 1. Support your spaceship game

In the Game Architecture course, you have been assigned the task of making a [Space Ship Shooter game](https://gitlab.com/Smet19/prog50016.Project1) and your game is going to require data. Designers do not like to work with text files and working in these can lead to errors. To support your designers and avoid errors you must develop a Windows application that will remove these issues.

- [ ] ENTITY EDITOR (70%)
You must develop a Windows application using WPF that is user friendly and easy to use so you can edit your game entities. Your design must remove all possible user error.
- User error is by allowing someone to manually enter data which can cause errors in your runtime code (example: color or colour?).
- This means the user shouldn’t be capable of entering incorrect data

- [ ] COMMON ACTIONS
- For each of the following sections, the user must be capable of loading and saving the game data. You cannot expect the user to recreate the game data each time they open the editor.
- [ ] GAME SETTINGS (10%)
- Instead of manually changing the game setting through a text editor, the user should be able to update them through your editor.
- Game setting should be driving your game as well (such as width, heigh, window name, and anything else)
- [ ] PLAYER (20%)
- Any attributes you require for your player ship should be editable here
- Texture, Lives, health, projectile damage (although you may want to write that to a separate file and read this in so your projectiles can be different?)
- Any other attributes that shouldn’t be hardcoded
- [ ] ENEMY SHIP (20%)
- Any attributes you require for the enemy ships should be editable here, remember if you are doing the advanced requirements you will need to support all your ships
- Texture, health, projectile damage (although you may want to write that to a separate file and read this in so your projectiles can be different?)
- Any other attributes that shouldn’t be hardcoded
- [ ] ASTEROIDS (20%)
- Any attributes you require for the asteroids should be editable here, remember if you are doing the advanced requirements you will need to support all your ships
- Texture, health, projectile damage (although you may want to write that to a separate file and read this in so your projectiles can be different?)
- Any other attributes that shouldn’t be hardcoded

## Part 2. Analyze your Spaceship Game

Analytics is a very important part of game development and you must support this in your Space Ship game.
- [ ] C++ SQL (15%)
You must log the user events for your game using SQLite. These events should be the following:
- 3% - Number of times the player died
- 3% - Number of times the player destroyed a space ship
- 3% - Number of times the player destroyed an asteroid
- 3% - Your choice, you must decide something to log here
- 3% - Your choice, you must decide something to log here
- [ ] VISUALIZE THE DATA (10%)
Since we have logged some data, we should display it! You must write another WPF application that will display the data saved from you C++ project.

## Part 3. HUD editor (5%)
Devise a way to let the user adjust the layout of your in-game HUD. To achieve this section, you must also create an in-game support for loading the data.
- The user must be able to adjust the position and size of the HUD item
- If it is a label, they must be able to change the font
- The editor must also show the user where the HUD items will be in your editor. This way they can get an idea what the layout will look like before running your game.
